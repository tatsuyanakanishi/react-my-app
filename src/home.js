import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import {Paper, Typography, Button } from "@material-ui/core";
import {useHistory} from 'react-router-dom';

//スタイルの設定
const useStyles = makeStyles({
    container:{height: '100vh'},
    content: {margin: '200px 150px'}
});
// 初期表示画面用のコンポーネント
const Home = (props) => {
    const classes = useStyles();
    //react-routerからHistoryオブジェクトを受け取る
    const history = useHistory();
    return (
        <Paper ClassName={classes.container}>
            <Typography variant='button'>やることリストに</Typography>
            <Button variant='outlined' onClick={() => history.push('/login')}>
                ログイン
            </Button>
        </Paper>
    );
};
export default Home;